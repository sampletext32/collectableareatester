﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableAreaController : MonoBehaviour
{
    private RectScaler _rectScaler;
    private TimeImpulser _spawningImpulser;
    public GameObject CollectableItemPrefab;
    public int ActiveItems = 0;
    public int MaximumItems = 10;

    private CollectableItemController[] _collectableItemControllers;

    private bool _isSpawning = false;

    public bool IsCollecting;

    void Awake()
    {
        _rectScaler = GetComponent<RectScaler>();
        _collectableItemControllers = new CollectableItemController[MaximumItems];
        LaunchSpawning();
    }

    public void LaunchSpawning()
    {
        _isSpawning = true;
        _spawningImpulser = TimeCounter.Get().Create(3f, CheckAndSpawn, false);
    }

    public void PauseSpawning()
    {
        _isSpawning = false;
        Destroy(_spawningImpulser);
        _spawningImpulser = null;
    }

    private int FindEmptyItemIndex()
    {
        for (int i = 0; i < MaximumItems; i++)
        {
            if (_collectableItemControllers[i] == null)
            {
                return i;
            }
        }

        return -1;
    }

    private void CheckAndSpawn()
    {
        Spawn();
        if (ActiveItems >= MaximumItems)
        {
            PauseSpawning();
        }
    }

    void Spawn()
    {
        float width = _rectScaler.GetWidth();
        float angle = Random.Range(0, 360f);
        float distance = Random.Range(0, width / 2);

        float x = distance * Mathf.Cos(angle);
        float y = distance * Mathf.Sin(angle);

        SpawnAtXY(x, y);

        ActiveItems++;
    }

    private void SpawnAtXY(float x, float y)
    {
        GameObject collectableItem = Instantiate(CollectableItemPrefab);
        var rectTransform = collectableItem.GetComponent<RectTransform>();
        collectableItem.transform.SetParent(gameObject.transform);
        rectTransform.anchoredPosition = new Vector2(x, y);
        rectTransform.localScale = Vector3.one;


        var itemController = collectableItem.GetComponent<CollectableItemController>();

        AssignSpawnedToIndex(itemController, FindEmptyItemIndex());
    }

    private void AssignSpawnedToIndex(CollectableItemController itemController, int index)
    {
        itemController.Index = index;
        itemController.TimeToCollect = 1f;
        itemController.OwnerAreaController = this;

        _collectableItemControllers[index] = itemController;
    }

    public void BeginCollectItem(int index)
    {
        if (IsCollecting)
        {
            Debug.Log("Error, Already Collecting");
            return;
        }

        Debug.Log("Collecting " + index);

        IsCollecting = true;

        var itemController = _collectableItemControllers[index];

        OnBeginCollect(itemController);

        TimeCounter.Get().Create(itemController.TimeToCollect, () =>
        {
            _collectableItemControllers[index] = null;
            ActiveItems--;
            IsCollecting = false;

            if (!_isSpawning)
            {
                LaunchSpawning();
            }

            OnEndCollect(itemController);
        });
    }

    private void OnBeginCollect(CollectableItemController itemController)
    {
        itemController.IsCollecting = true;
        itemController.SliderProgressObject.SetActive(true);
    }

    private void OnEndCollect(CollectableItemController itemController)
    {
        //Inventory.Add(itemController.Item);
        Destroy(itemController.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
    }
}