﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CollectableItemController : MonoBehaviour, IPointerDownHandler
{
    //ItemParameters
    public float TimeToCollect;
    public int ItemType;

    //Data for Spawner
    public bool IsCollecting = false;

    //System data for Unity
    public int Index;
    public CollectableAreaController OwnerAreaController;
    public GameObject SliderProgressObject;
    public Slider SliderProgress;
    private float _elapsed;

    public void Update()
    {
        if (IsCollecting)
        {
            _elapsed += Time.deltaTime;
            SliderProgress.value = _elapsed / TimeToCollect;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        OwnerAreaController.BeginCollectItem(Index);
    }
}
