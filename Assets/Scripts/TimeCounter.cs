﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class TimeCounter : MonoBehaviour
{
    private static TimeCounter _instance;

    public static TimeCounter Get()
    {
        return _instance;
    }

    void Awake()
    {
        _instance = this;
    }

    public TimeImpulser Create(float period, Action onImpulse, bool singleTime = true)
    {
        var timeImpulser = gameObject.AddComponent<TimeImpulser>();
        timeImpulser.PeriodTime = period;
        timeImpulser.OnImpulse = onImpulse;
        timeImpulser.SingleTime = singleTime;
        return timeImpulser;
    }
}