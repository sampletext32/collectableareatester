﻿using System;
using UnityEngine;

public class TimeImpulser : MonoBehaviour
{
    public float PeriodTime;
    private float _elapsed;

    public Action OnImpulse;

    public bool SingleTime;
    
    void Update()
    {
        _elapsed += Time.deltaTime;
        if (_elapsed > PeriodTime)
        {
            OnImpulse?.Invoke();
            if (SingleTime)
            {
                Destroy(this);
            }
            _elapsed = 0;
        }
    }
}
